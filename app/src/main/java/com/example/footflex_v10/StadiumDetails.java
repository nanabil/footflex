package com.example.footflex_v10;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import java.time.LocalDate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.type.DateTime;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class StadiumDetails extends AppCompatActivity {
    Button reserve;
    String stadiumName;
    List<Timestamp> timestampList;
    List<Timestamp> timestampListFilter;
    String stadiumId;
    FirebaseFirestore db;
    TextView textViewStadiumName;
    CalendarView calendarView;
    TimestampAdapter adapter;
    int anne,mois,jour;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stadium_details);
        reserve = findViewById(R.id.reserve);
        db = FirebaseFirestore.getInstance();
        textViewStadiumName = findViewById(R.id.textViewStadiumName);
        calendarView = findViewById(R.id.calendarView);

        LocalDate today = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            today = LocalDate.now();
        }

        // Extract year, month, and day of the month
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            anne = today.getYear();
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            mois = today.getMonthValue() - 1; // 1-based (January is 1)
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            jour = today.getDayOfMonth();
        }

        Intent intent = getIntent();
        if (intent != null) {
            stadiumName = intent.getStringExtra("stadiumName");
            timestampList = getIntent().getParcelableArrayListExtra("timestampList");
            stadiumId = getIntent().getStringExtra("Id");
        }
        timestampListFilter = new ArrayList<>();
        for (Timestamp ts : timestampList) {
            timestampListFilter.add(new Timestamp(ts.toDate()));
        }
        textViewStadiumName.setText(stadiumName);
        reserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openTimePicker(stadiumId,anne,mois,jour);
            }
        });
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                // Add your logic here to filter data based on the selected date
                // For example:
                Calendar selectedDate = Calendar.getInstance();
                selectedDate.set(year, month, dayOfMonth);
                Timestamp selectedTimestamp = new Timestamp(selectedDate.getTime());
                filterData(selectedTimestamp);
            }
        });
        // After initializing the ListView
        ListView listView = findViewById(R.id.listView);
        List<Timestamp> tmp = new ArrayList<>();

        // Create the adapter and set it to the ListView
        adapter = new TimestampAdapter(this, tmp);
        listView.setAdapter(adapter);

        // Fetch the stadium details passed from the previous activity

    }


    private void openTimePicker(String id,int year ,int month , int day){

        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                Calendar selectedDateTime = Calendar.getInstance();
                selectedDateTime.set(year, month, day, hour,minute);
                //Showing the picked value in the textView
                Timestamp selectedTimestamp = new Timestamp(selectedDateTime.getTime());
                updateFirestoreWithTimestamp(selectedTimestamp,id);
                updateUserReservations(year, month, day, hour,minute);


            }
        }, 15, 30, false);

        timePickerDialog.show();
    }

    private void updateFirestoreWithTimestamp(Timestamp timestamp,String stadiumId) {
        // Assuming stadiumId holds the ID of the stadium
        //Log.d("StadiumDetails", "Stadium ID: " + stadiumId);

        // Retrieve the document reference
        DocumentReference docRef = db.collection("MAP").document(stadiumId);

        // Update the "free" array field with the new timestamp
        docRef.update("free", FieldValue.arrayUnion(timestamp))
                .addOnSuccessListener(aVoid -> {
                    // Update successful
                    Toast.makeText(StadiumDetails.this, "Timestamp added successfully", Toast.LENGTH_SHORT).show();
                    // You can add any UI update or further actions here
                })
                .addOnFailureListener(e -> {
                    // Handle any errors
                    Toast.makeText(StadiumDetails.this, "Failed to add timestamp", Toast.LENGTH_SHORT).show();
                });
    }

    public void updateUserReservations(int year, int month, int day, int hour, int minute){
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            String userID = currentUser.getUid();

            // Assuming you have a DatabaseReference instance
            DatabaseReference usersRef = FirebaseDatabase.getInstance("https://footflex-88c63-default-rtdb.europe-west1.firebasedatabase.app/").getReference("Users").child(userID).child("reservations");

            // Assuming 'formattedTimestamp' holds the date of the reservation
            String formattedTimestamp = year + " " +month + " "+day + " "+hour + " "+minute;

            // Push the reservation date to the user's reservation list
            usersRef.push().setValue(formattedTimestamp)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            // Reservation saved successfully
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            // Handle failure
                        }
                    });
        }

    }
    // Create a custom adapter
    public class TimestampAdapter extends ArrayAdapter<Timestamp> {
        private List<Timestamp> timestampList;
        private Context context;

        public TimestampAdapter(Context context, List<Timestamp> timestampList) {
            super(context, 0, timestampList);
            this.context = context;
            this.timestampList = timestampList;

        }



        public void AddTime(List<Timestamp> list) {
            this.timestampList.clear();
            for(Timestamp x : list){
                timestampList.add(x);
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;

            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.reservation_layout, parent, false);

                viewHolder = new ViewHolder();
                viewHolder.button = convertView.findViewById(R.id.button);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            // Get the timestamp at the current position
            Timestamp timestamp = timestampList.get(position);

            // Set the text of the button with the timestamp
            if (timestamp != null) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
                String formattedTimestamp = dateFormat.format(timestamp.toDate());
                viewHolder.button.setText(formattedTimestamp);
            }

            return convertView;
        }

        class ViewHolder {
            Button button;
        }
    }
    private void filterData(Timestamp selectedTimestamp) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        List<Timestamp> filteredTimestamps = new ArrayList<>();

        cal1.setTime(selectedTimestamp.toDate());
        // Iterate over the timestamp list
        for (Timestamp x : timestampListFilter) {
            cal2.setTime(x.toDate());
            //Log.d("MainActivity", "This is a debug message " + cal2.get(Calendar.DAY_OF_YEAR));
            if(cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR)
                    && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)){

                filteredTimestamps.add(x);
            }
        }
        adapter.AddTime(filteredTimestamps);
        // Notify the adapter about the data change
        adapter.notifyDataSetChanged();
    }



}







/*
            DatePicker datePicker = findViewById(R.id.calendarView);
            datePicker.init(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(),
                    (view, year, monthOfYear, dayOfMonth) -> {
                        Calendar selectedDate = Calendar.getInstance();
                        selectedDate.set(year, monthOfYear, dayOfMonth);

                        // Use a HashSet to store unique hours for the selected date
                        HashSet<Integer> hoursSet = new HashSet<>();

                        // Filter timestamps for the selected date
                        for (Timestamp timestamp : timestampList) {
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(timestamp.toDate());

                            // Check if the timestamp corresponds to the selected date
                            if (calendar.get(Calendar.YEAR) == year &&
                                    calendar.get(Calendar.MONTH) == monthOfYear &&
                                    calendar.get(Calendar.DAY_OF_MONTH) == dayOfMonth) {

                                // Extract hour
                                int hour = calendar.get(Calendar.HOUR_OF_DAY);

                                // Store the unique hour in the set
                                hoursSet.add(hour);
                            }
                        }

                        // Show hours in a Toast
                        if (!hoursSet.isEmpty()) {
                            StringBuilder hoursText = new StringBuilder("Hours for selected day: ");
                            for (int hour : hoursSet) {
                                hoursText.append(hour).append(", ");
                            }
                            Toast.makeText(getApplicationContext(), hoursText.toString(), Toast.LENGTH_SHORT).show();
                        }

                        // Here you could add logic to mark these hours in a different color on the DatePicker
                        // Unfortunately, DatePicker doesn't provide a straightforward way to change individual day/hour colors.
                    });
            if (stadiumName != null) {
                // Update UI elements with stadium details (e.g., TextViews)
                TextView nameTextView = findViewById(R.id.textViewStadiumName);
                nameTextView.setText(stadiumName);

                // Add more details if needed
            }*/