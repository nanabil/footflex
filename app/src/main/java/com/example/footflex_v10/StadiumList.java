package com.example.footflex_v10;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class StadiumList extends AppCompatActivity {

    private ListView listView;
    private StadiumAdapter adapter;
    private List<DocumentSnapshot> documentList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stadium_list);

        listView = findViewById(R.id.listView);

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        // Fetch data from Firestore
        db.collection("MAP")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (DocumentSnapshot document : task.getResult()) {
                            // Add each document to the list
                            documentList.add(document);
                        }
                        // Set the adapter after data retrieval
                        adapter = new StadiumAdapter(this, documentList);
                        listView.setAdapter(adapter);

                        // Handle item click to open StadiumDetailsActivity
                        listView.setOnItemClickListener((adapterView, view, position, id) -> {
                            DocumentSnapshot selectedDocument = documentList.get(position);
                            String stadiumName = selectedDocument.getString("name");
                            List<Timestamp> timestampList = new ArrayList<>();
                            Object freeField = selectedDocument.get("free");
                            String documentId = selectedDocument.getId();
                            if (freeField instanceof List<?>) {
                                for (Object obj : (List<?>) freeField) {
                                    if (obj instanceof Timestamp) {
                                        timestampList.add((Timestamp) obj);

                                    }
                                }
                            }
                            if (stadiumName != null) {
                                Intent intent = new Intent(StadiumList.this, StadiumDetails.class);
                                intent.putExtra("stadiumName", stadiumName);
                                intent.putExtra("timestampList", new ArrayList<>(timestampList));
                                intent.putExtra("Id",documentId );
                                startActivity(intent);
                            }
                        });

                    } else {
                        Log.d("error:", "Failed to fetch data from Firestore");
                    }
                });
    }

}