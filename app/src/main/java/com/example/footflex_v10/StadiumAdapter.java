package com.example.footflex_v10;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.footflex_v10.R;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.squareup.picasso.Picasso;

import java.util.List;

public class StadiumAdapter extends BaseAdapter {

    private Context context;
    private List<DocumentSnapshot> stadiumList;
    private FirebaseStorage storage;

    public StadiumAdapter(Context context, List<DocumentSnapshot> stadiumList) {
        this.context = context;
        this.stadiumList = stadiumList;
        this.storage = FirebaseStorage.getInstance();
    }

    @Override
    public int getCount() {
        return stadiumList.size();
    }

    @Override
    public Object getItem(int position) {
        return stadiumList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.stadium_layout, parent, false);
        }

        ImageView stadiumImage = convertView.findViewById(R.id.stadiumImage);
        TextView stadiumName = convertView.findViewById(R.id.stadiumName);

        DocumentSnapshot currentItem = stadiumList.get(position);

        // Assuming the field names in Firestore are "imageUrl" and "name"
        String imageURL = currentItem.getString("imageUrl");
        String name = currentItem.getString("name");
        stadiumName.setText(name);

        if (imageURL != null) {
            // Convert the gs:// URL to https:// URL
            String httpsURL = imageURL.replace("gs://", "https://firebasestorage.googleapis.com/v0/b/footflex-88c63.appspot.com/o/");

            // Load image into ImageView using Picasso
            Picasso.get()
                    .load(httpsURL)
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .into(stadiumImage);
        }



        return convertView;
    }
}
