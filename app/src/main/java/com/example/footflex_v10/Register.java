package com.example.footflex_v10;

import static android.app.PendingIntent.getActivity;
import static androidx.constraintlayout.helper.widget.MotionEffect.TAG;

import static com.example.footflex_v10.utils.FirebaseUtil.allUserCollectionReference;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.Firebase;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import android.Manifest;



import java.util.HashMap;

public class Register extends AppCompatActivity {
    TextInputEditText editTextEmail ,editTextPassword , editTextPhotoUrl, editTextAge, editFirstName ,editLastName;
    private Uri selectedImageUri;
    ImageView userPhoto;
    Button btnReg ;
    FirebaseAuth mAuth;
    DatabaseReference reference;
    ProgressBar progressBar;
    TextView clickToLogin;

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }
    }



    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        editTextEmail = findViewById(R.id.email) ;
        editTextPassword = findViewById(R.id.password) ;
        editFirstName = findViewById(R.id.firstName) ;
        editLastName = findViewById(R.id.lastName) ;
        editTextAge = findViewById(R.id.age) ;
        userPhoto = findViewById(R.id.userPhoto);
        btnReg = findViewById(R.id.btn_register) ;
        mAuth = FirebaseAuth.getInstance();
        clickToLogin = findViewById(R.id.LoginNow);

        userPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkPermissions()) {
                    // Permissions are not granted, request them from the user
                    ActivityCompat.requestPermissions(Register.this,
                            new String[]{Manifest.permission.CAMERA,
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);
                } else {
                    // Permissions granted, open an image picker (gallery)
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    galleryIntent.setType("image/*");
                    startActivityForResult(galleryIntent, 1); // Use a unique request code
                }
            }
        });



        clickToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),Login.class);
                startActivity(intent);
                finish();
            }
        });

        progressBar = findViewById(R.id.RegisterProgressBar);
        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                String email , password ;
                email = editTextEmail.getText().toString();
                password = editTextPassword.getText().toString();
                if(TextUtils.isEmpty(email)){
                    Toast.makeText(Register.this,"Enter Email", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(password)){
                    Toast.makeText(Register.this,"Enter Password", Toast.LENGTH_SHORT).show();
                    return;
                }
                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener( new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                progressBar.setVisibility(View.GONE);
                                if (task.isSuccessful()) {
                                    FirebaseUser user = mAuth.getCurrentUser();
                                    //add user to real time database
                                    String userId = user.getUid();
                                    String firstName ,lastName , age;
                                    firstName = editFirstName.getText().toString();
                                    lastName = editLastName.getText().toString();
                                    age = editTextAge.getText().toString();
                                    User userInfo = new User(firstName,lastName,email,selectedImageUri.toString(),age);
                                    reference = FirebaseDatabase.getInstance("https://footflex-88c63-default-rtdb.europe-west1.firebasedatabase.app/").getReference("Users");
                                    CollectionReference usersRef = allUserCollectionReference();
                                    reference.child(userId).setValue(userInfo).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Log.d(TAG, "User data added successfully");
                                            } else {
                                                Log.e(TAG, "Error adding user data: " + task.getException().getMessage());
                                            }
                                        }
                                    });

                                    usersRef.add(userInfo)
                                            .addOnSuccessListener(documentReference -> {
                                                Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                                                // Handle success, if needed
                                            })
                                            .addOnFailureListener(e -> {
                                                Log.w(TAG, "Error adding document", e);
                                                // Handle failure, if needed
                                            });

                                    // Upload the image to Firebase Storage
                                    StorageReference storageRef = FirebaseStorage.getInstance().getReference().child("profile_images").child(userId + ".jpg");

                                    storageRef.putFile(selectedImageUri)
                                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                                @Override
                                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                    // Image uploaded successfully, now get the download URL
                                                    storageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                        @Override
                                                        public void onSuccess(Uri uri) {
                                                            // Get the image download URL
                                                            String imageUrl = uri.toString();

                                                            // Set the user's profile image URL in the Realtime Database
                                                            DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference().child("Users").child(userId);
                                                            usersRef.child("photo").setValue(imageUrl)
                                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                        @Override
                                                                        public void onComplete(@NonNull Task<Void> task) {
                                                                            if (task.isSuccessful()) {
                                                                                Log.d(TAG, "User image URL added successfully");
                                                                                // Proceed with other operations or navigation
                                                                            } else {
                                                                                Log.e(TAG, "Error adding user image URL: " + task.getException().getMessage());
                                                                            }
                                                                        }
                                                                    });
                                                        }
                                                    });
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    // Handle unsuccessful image upload
                                                    Log.e(TAG, "Image upload failed: " + e.getMessage());
                                                }
                                            });

                                    Toast.makeText(Register.this, "Account Created.",
                                            Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                    finish();

                                } else {
                                    // If sign in fails, display a message to the user.
                                    Toast.makeText(Register.this, "Authentication failed.",
                                            Toast.LENGTH_SHORT).show();

                                }
                            }
                        });
            }
        });

    }
    private boolean checkPermissions() {
        int cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int readStoragePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeStoragePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        return cameraPermission == PackageManager.PERMISSION_GRANTED &&
                readStoragePermission == PackageManager.PERMISSION_GRANTED &&
                writeStoragePermission == PackageManager.PERMISSION_GRANTED;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            // Get the selected image URI
            selectedImageUri = data.getData();

            // Here, you can display the selected image in the ImageView for preview if needed
            userPhoto.setImageURI(selectedImageUri);



        }
    }





}