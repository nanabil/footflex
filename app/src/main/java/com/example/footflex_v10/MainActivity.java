package com.example.footflex_v10;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.bumptech.glide.Glide;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;


public class MainActivity extends AppCompatActivity {
    FirebaseAuth auth;
    ImageView userProfileImage;
    Button logOut;
    Button stadiumList;
    TextView showMail;
    FirebaseUser user;
    Button GoMap,Users;
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        auth = FirebaseAuth.getInstance();
        userProfileImage = findViewById(R.id.userProfileImage);
        logOut = findViewById(R.id.logout);
        stadiumList = findViewById(R.id.stadiumList);
        showMail = findViewById(R.id.user_information);
        Users = findViewById(R.id.users);
        GoMap = findViewById(R.id.goMap);
        user =auth.getCurrentUser();
        if(user==null ){
            Intent intent = new Intent(getApplicationContext(),Login.class);
            startActivity(intent);
            finish();
        }else{
            showMail.setText(user.getEmail());
            loadUserProfileImage(user);
            userProfileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Handle click on the profile image (e.g., view profile, open settings, etc.)
                    // Add your logic here
                }
            });

        }
        stadiumList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),StadiumList.class);
                startActivity(intent);

            }
        });
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(getApplicationContext(),Login.class);
                startActivity(intent);
                finish();
            }
        });
        GoMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,Map.class);
                startActivity(intent);
            }
        });
        Users.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,MainActivity2.class);
                startActivity(intent);
            }
        });



    }
    private void loadUserProfileImage(FirebaseUser currentUser) {
        // Get the current user
        if (currentUser != null) {
            String userId = currentUser.getUid();

            // Load the user's profile image URL from the Realtime Database
            DatabaseReference usersRef = FirebaseDatabase.getInstance("https://footflex-88c63-default-rtdb.europe-west1.firebasedatabase.app/").getReference().child("Users").child(userId);
            usersRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()) {
                        User user = snapshot.getValue(User.class);
                        if (user != null) {
                            String imageUrl = user.getPhoto();
                            Log.d("myTag", "imageUrl: " +imageUrl);

                            // Use a library like Picasso or Glide to load the image into the ImageView
                            if (!TextUtils.isEmpty(imageUrl)) {
                                // Load the image into the ImageView using Glide or Picasso
                                StorageReference storageRef = FirebaseStorage.getInstance().getReference("profile_images/" +userId +".jpg" );
                                try {
                                    File localFile = File.createTempFile("tempfile",".jpg");
                                    storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                        @Override
                                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                            Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                                            Bitmap circularBitmap = getCircularBitmap(bitmap);

// Set the circular bitmap to your ImageView
                                            if (circularBitmap != null) {
                                                userProfileImage.setImageBitmap(circularBitmap);
                                            }else {
                                                userProfileImage.setImageBitmap(bitmap);
                                            }

                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(MainActivity.this, "Failed To Load The User Photo.",
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }

                               // Glide.with(MainActivity.this).load(pathReference)
                                //        .placeholder(R.drawable.ic_launcher_foreground)
                                //        .into(userProfileImage);
                                //Glide.with(MainActivity.this).load(imageUrl).placeholder(R.drawable.ic_launcher_foreground).into(userProfileImage);

                                // Show the profile image view
                                userProfileImage.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    // Handle errors
                }
            });
        }
    }
    private Bitmap getCircularBitmap(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        int size = Math.min(width, height);

        Bitmap output = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(output);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setShader(new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));

        float radius = size / 2f;
        canvas.drawCircle(radius, radius, radius, paint);

        return output;
    }
}